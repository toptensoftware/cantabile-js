/**
 * An anonymous type representing an active key range
 *
 * @class KeyRange
 */

/** 
 * The MIDI note number of the lowest note in the key range
 * @property min
 * @type {Number}
 */

/** 
 * The MIDI note number of the highest note in the key range
 * @property max
 * @type {Number}
 */

/** 
 * The transpose setting associated with the key range (in semi-tones)
 * @property transpose
 * @type {Number}
 */

/** 
 * The color associated with the key range (0 - 15)
 * @property color
 * @type {Number}
 */


 /** 
 * The title of the key range (ie: name of the target)
 * @property title
 * @type {String}
 */

