/**
 * An anonymous type representing a state
 *
 * @class State
 */

/** 
 * The name of the state
 * @property name
 * @type {String}
 */

/** 
 * The zero based program number of the state
 * @property pr
 * @type {Number}
 */

/** 
 * The color of the state (0 to 15)
 * @property color
 * @type {Number}
 */

