/**
 * An anonymous type representing a color table entry
 *
 * @class ColorEntry
 */

/** 
 * The foreground color in #RRGGBBAA format
 * @property fore
 * @type {String}
 */

/** 
 * The background color in #RRGGBBAA format
 * @property back
 * @type {String}
 */

