/**
 * An anonymous type representing a item (either a song or break) in a set list
 *
 * @class SetListItem
 */

/** 
 * The item kind - either "song" or "break"
 * @property kind
 * @type {String}
 */

/** 
 * The name of the song or break
 * @property name
 * @type {String}
 */

/** 
 * The zero based program number of a song
 * @property pr
 * @type {Number}
 */

/** 
 * The color of the item (0 to 15)
 * @property color
 * @type {Number}
 */

