YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "Application",
        "Binding4Watcher",
        "BindingParam",
        "BindingPointEntry4",
        "BindingPointInfo",
        "BindingPointInfo4",
        "BindingWatcher",
        "Bindings",
        "Bindings4",
        "Cantabile",
        "ColorEntry",
        "CommandInfo",
        "Commands",
        "ControllerWatcher",
        "EndPoint",
        "Engine",
        "KeyRange",
        "KeyRanges",
        "MidiControllerEvent",
        "OnscreenKeyboard",
        "PatternWatcher",
        "SetList",
        "SetListItem",
        "ShowNote",
        "ShowNotes",
        "Song",
        "SongStates",
        "State",
        "States",
        "Transport",
        "Variables"
    ],
    "modules": [],
    "allModules": [],
    "elements": []
} };
});