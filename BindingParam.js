/**
 * An anonymous type that describes a binding point parameter
 *
 * @class BindingParam
 */

/** 
 * The name of the parameter
 * @property name
 * @type {String}
 */

/** 
 * The type of the parameter
 * @property type
 * @type {String}
 */

