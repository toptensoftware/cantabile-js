/**
 * An anonymous type representing a show note
 *
 * @class ShowNote
 */

/** 
 * The item kind - either "song" or "break"
 * @property kind
 * @type {String}
 */

/** 
 * The name of the song or break
 * @property name
 * @type {String}
 */

/** 
 * The zero based program number of a song
 * @property pr
 * @type {Number}
 */

/** 
 * The color of the item (0 to 15)
 * @property color
 * @type {Number}
 */


/**
 * Background color (0 - 15)
 * @property backgroundColor
 * @type {Number}  
 */

/**
 * Bold font
 * @property bold
 * @type {Boolean} 
 */

/**
 * Fixed pitch font
 * @property fixedPitch
 * @type {Boolean}
 */

/**
 * Font size
 * @property fontSize
 * @type {Number}  
 */

/**
 * True if the show note is currently hidden
 * @property hidden
 * @type {Boolean} 
 */

/**
 * URL to retrieve the note's background image (or null)
 * @property imageUrl
 * @type {String}  
 */

/**
 * How much to scale the image by (0.0 - 1.0)
 * @property imageScale
 * @type {Number}  
 */

/**
 * Width of the image in pixels
 * @property imageWidth
 * @type {Number}  
 */

/**
 * Height of the image in pixels
 * @property imageHeight
 * @type {Number}  
 */

/**
 * The show note's text
 * @property text
 * @type {String}  
 */

/**
 * Text alignment ("left", "center" or "right")
 * @property textAlign
 * @type {String} 
 */

/**
 * Text color (0 - 15)
 * @property textColor
 * @type {Number}  
 */
