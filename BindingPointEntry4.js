/**
 * An anonymous type that describes a binding point entry as returned from {{#crossLink "Bindings4/availableBindingPoints:method"}}{{/crosslink}}
 *
 * @class BindingPointEntry4
 */

/** 
 * The id of the bindable object
 * @property bindableId
 * @type {String}
 */

/** 
 * The id of the binding point on the bindable object
 * @property bindingPointId
 * @type {String}
 */

/** 
 * The display name of the binding point
 * @property displayName
 * @type {String}
 */

/** 
 * Indicates if this binding point can be used as a source binding point
 * @property isSource
 * @type {Boolean}
 */

/** 
 * Indicates if this binding point can be used as a target binding point
 * @property isTarget
 * @type {Boolean}
 */


