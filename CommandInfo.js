/**
 * An anonymous type that describes a command
 *
 * @class CommandInfo
 */

/** 
 * The id of the command
 * @property id
 * @type {String}
 */

/** 
 * The display name of the command
 * @property name
 * @type {String}
 */

